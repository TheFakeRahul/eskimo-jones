﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureManager : MonoBehaviour
{

    public LayerMask playerLayer;
    private bool collectedTreasure;
    private float timeActivated;

    void OnEnable()
    {
        collectedTreasure = false;
        timeActivated = 0;
    }

    void Update()
    {
        timeActivated += Time.deltaTime;
		if (!collectedTreasure && timeActivated >= 1.0f) {
            if(Physics2D.OverlapBox(transform.position, new Vector2(0.8f, 0.8f), 0, playerLayer)) {
                GameManager.Instance.IncrementLevel();
                print("Collected treasure");
                GetComponent<AudioSource>().Play();
                collectedTreasure = true;
			}
		}
    }
}
