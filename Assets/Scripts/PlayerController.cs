﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [Header("Movement Stuff")]
    public float iceMoveSpeed;
    public float groundMoveSpeed;
    private float moveSpeed;
    private bool isMoving;
    private Vector2 lastDir;

    [Header("Important Layers")]
    public LayerMask groundLayer;
    public LayerMask collisionLayers;

    [Header("Sound FX")]
    public AudioClip bumpSound;
    public AudioClip slideSound;
    private AudioSource audioSource;

    private Rigidbody2D rb;

    public static PlayerController Instance;
    void Awake() {
        if (Instance == null) Instance = this;
        else Destroy(this);
	}

    void Start(){
        audioSource = GetComponent<AudioSource>();
        moveSpeed = 0;
        lastDir = Vector2.zero;
        isMoving = false;
        rb = GetComponent<Rigidbody2D>();
        Physics2D.queriesStartInColliders = false;
    }

    private Vector2 MoveDirection() {
        if (GameManager.Instance != null && !GameManager.Instance.CanMove) return Vector2.zero;
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) return new Vector2(0, 1);
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) return new Vector2(-1, 0);
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) return new Vector2(0, -1);
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) return new Vector2(1, 0);
        return Vector2.zero;
    }

    void Update()
    {
        if(isMoving) return;

        StartCoroutine(MovePlayer(MoveDirection()));
    }

    public void Reset(Vector2 pos) {
        isMoving = false;
        StartCoroutine(ResetPlayer(pos));
	}

    private IEnumerator ResetPlayer(Vector2 pos) {
        yield return new WaitForSeconds(0.1f);
        lastDir = Vector2.zero;
        transform.position = pos;
    }

    private IEnumerator MovePlayer(Vector2 dir){
        if(dir != Vector2.zero) lastDir = dir;
        yield return null;
        if (dir != Vector2.zero && !CheckCollision(dir)) {
            lastDir = dir;
            isMoving = true;
            PlayAudio(slideSound);
            while(isMoving){
                Vector2 startPos = transform.position;
                SetMoveSpeed(dir);
                float moveTime = 1.0f / moveSpeed;  // The time taken to move one unit
                float timer = 0.0f;
                while(timer < moveTime){
                    SetVelocity(dir * moveSpeed);
                    timer += Time.deltaTime;
                    yield return null;
                }
                SetVelocity(Vector2.zero);
                transform.position = startPos + dir;
                if (!IsOnIce()) break;
                else if (CheckCollision(dir)) {
                    PlayAudio(bumpSound);
                    break;
                }
            }
            isMoving = false;
        }
    }

    private void PlayAudio(AudioClip clip) {
        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
	}

    private void SetMoveSpeed(Vector2 dir) {
        moveSpeed = IsOnIce() ? iceMoveSpeed : groundMoveSpeed;
        if (GoingOnIce(dir)) moveSpeed = iceMoveSpeed; 
	}

    // Check if player is currently on ice
    private bool IsOnIce(){
        return !Physics2D.OverlapCircle(transform.position, 0.4f, groundLayer);
    }

    private bool GoingOnIce(Vector2 dir) {
        return !Physics2D.OverlapCircle((Vector2)transform.position + dir, 0.4f, groundLayer);
    }

    // Check if player colliding in specified direction
    private bool CheckCollision(Vector2 dir){
        return Physics2D.Raycast(transform.position, dir, 1.0f, collisionLayers);
    }

    private void SetVelocity(Vector2 vel){
        rb.velocity = vel;
    }

    public Vector2 GetDir() {
        return lastDir;
	}

    public bool GetIsMoving => isMoving;
}
