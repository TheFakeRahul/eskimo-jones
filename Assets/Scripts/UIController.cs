﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    [Header("Main Menu")]
    public Image menuBackground;
    public Text menuTitleBackground;
    public Text menuTitleForeground;
    public Text beginText;
    public Text[] creditsTexts;
    private bool animateMenu;

    [Header("Introduction Text")]
    public Image introBackground;
    public Text introText;

    [Header("Outro Text")]
    public Image outroBackground;
    public Text outroTextMain;
    public Text outroTextSub;

    [Header("Transition Elements")]
    public Image transitionImage;
    private bool startedTransition;

    public static UIController Instance;
    void Awake() {
        if (Instance == null) Instance = this;
        else Destroy(this);

        animateMenu = false;
        startedTransition = false;
    }

    public void EnableMenu(bool enabled) {
        menuBackground.enabled = beginText.enabled = enabled;
        menuTitleBackground.enabled = menuTitleForeground.enabled = enabled;
        foreach(Text credit in creditsTexts) {
            credit.enabled = enabled;
		}
        animateMenu = enabled;
        if (enabled) StartCoroutine(MenuStartTextEffect());
	}

    public void EnableIntroText(bool enabled) {
        introBackground.enabled = enabled;
        introText.enabled = enabled;
        if (enabled) StartCoroutine(IntroTextEffect());
	}

    private IEnumerator IntroTextEffect() {
        float alpha = 0;
        introText.color = new Color(1, 1, 1, alpha);
        while(alpha < 1.0f) {
            alpha += Time.deltaTime / 2.0f;
            introText.color = new Color(1, 1, 1, alpha);
            yield return null;
        }
	}

    public void EnableOutroText(bool enabled) {
        outroBackground.enabled = enabled;
        outroTextMain.enabled = outroTextSub.enabled = enabled;
        if (enabled) StartCoroutine(OutroTextEffect());
    }

    private IEnumerator OutroTextEffect() {
        float alpha = 0;
        introText.color = new Color(1, 1, 1, alpha);
        while (alpha < 2.0f) {
            alpha += Time.deltaTime / 2.0f;
            outroTextMain.color = new Color(1, 1, 1, alpha);
            outroTextSub.color = new Color(1, 0, 0, alpha - 1.0f);
            yield return null;
        }
    }

    public void StartTransition() {
        if (!startedTransition) StartCoroutine(SceneTransition());
	}

    private void EnableTransitionImage(bool enabled) {
        transitionImage.enabled = enabled;
        SetTransitionImagePosition(new Vector2(0, 900));
        startedTransition = enabled;
	}

    private void SetTransitionImagePosition(Vector2 pos) {
        transitionImage.rectTransform.anchoredPosition = pos;
	}

    private IEnumerator SceneTransition() {
        EnableTransitionImage(true);
        float t = 1.0f;
        while (t > 0.0f) {
            float posY = 900.0f * (t - (1.0f - t));
            SetTransitionImagePosition(new Vector2(0, posY));
            t -= Time.deltaTime;
            yield return null;
		}
        EnableTransitionImage(false);
        yield return null;
	}

    private IEnumerator MenuStartTextEffect() {
        float t = 0.0f;
        int multipler = 1;
        Color textColour = beginText.color;
		while (animateMenu) {
            if (t < 0.0f) {
                multipler = 1;
                beginText.text = (beginText.text == "INSERT COIN TO START") ? "PRESS SPACE TO START" : "INSERT COIN TO START";
			}
            else if (t > 1.0f) multipler = -1;
            t += Time.deltaTime * multipler;
            beginText.color = new Color(textColour.r, textColour.g, textColour.b, t);
            yield return null;
		}
	}
}
