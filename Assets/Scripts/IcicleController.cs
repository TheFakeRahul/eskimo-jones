﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcicleController : MonoBehaviour
{

    public LayerMask playerLayer;
    public bool appearOnImpact;
    private bool showIcicle;

    private SpriteRenderer sprite;

    void Start() {
        showIcicle = !appearOnImpact;
        sprite = GetComponent<SpriteRenderer>();
        ShowSprite();
	}

    void Update()
    {
		if (appearOnImpact && !showIcicle) {
            if(Physics2D.OverlapBox(transform.position, transform.localScale * 0.9f, transform.rotation.eulerAngles.z, playerLayer)) {
                if(!PlayerController.Instance.GetIsMoving) showIcicle = true;
                ShowSprite();
			}
		}

		if (showIcicle) {
            if (Physics2D.OverlapBox(transform.position, transform.localScale * 0.9f, transform.rotation.eulerAngles.z, playerLayer)) {
                if (!PlayerController.Instance.GetIsMoving) {
                    if (GameManager.Instance.CurrentLevel < GameManager.Instance.GetLevelsLength - 1) GameManager.Instance.ResetLevel();
                    else GameManager.Instance.GameOver();
                }
			}
        }
    }

    private void ShowSprite() {
        sprite.enabled = showIcicle;
	}
}
