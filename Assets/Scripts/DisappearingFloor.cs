﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearingFloor : MonoBehaviour
{

    public LayerMask playerLayer;
    private bool playerOnIce;

    void Start() {
        playerOnIce = false;
	}

    void Update() {
        if (Physics2D.OverlapCircle(transform.position, 0.4f, playerLayer)) {
            if (!playerOnIce) StartCoroutine(Disappear());
            //else kill player
        }
	}

    private IEnumerator Disappear() {
        playerOnIce = true;
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();
        Color startColour = sprite.color;
        float t = 1.0f;
        while(t > 0.0f) {
            transform.localScale = new Vector2(t, t);
            sprite.color = Color.Lerp(startColour, Color.black, 1 - t);
            t -= Time.deltaTime * 2;
            yield return null;
		}
	}
}
