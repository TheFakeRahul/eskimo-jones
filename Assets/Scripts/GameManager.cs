﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private bool isMainMenu;
    private bool showStartText;
    private bool showEndText;
    private bool canMove;


    [Header("Music Tracks")]
    public AudioClip mainTrack;
    public AudioClip windTrack;
    public float maxVolume = 0.2f;
    private AudioSource audioSource;
    private bool transitioningMusic;

    [System.Serializable]
    public struct Level {
        public GameObject levelContainer;
        public Vector2 playerStartPos;
    }

    [Header("Levels")]
    public Level[] levels;
    private int currentLevel;

    public static GameManager Instance;
    void Awake() {
        if (Instance == null) Instance = this;
        else Destroy(this);
        currentLevel = 0;
    }

    void Start() {
        isMainMenu = true;
        showStartText = false;
        showEndText = false;
        canMove = false;

        UIController.Instance.EnableMenu(true);
        UIController.Instance.EnableIntroText(false);
        UIController.Instance.EnableOutroText(false);

        audioSource = GetComponent<AudioSource>();
        UpdateBackgroundMusic();
    }

    void Update() {
        if (isMainMenu) {
            canMove = false;
            if (Input.GetKeyDown(KeyCode.Space)) {
                if (!showStartText) {
                    ShowStartText();
                } else {
                    StartGame();
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape)) {
                Application.Quit();
            }
        } else {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                GameOver();
            }
            if (!WithinLevel() && canMove) {
                ResetLevel();
            }
            if(showEndText && Input.GetKeyDown(KeyCode.Space)) {
                GameOver();
			}
        }
    }

    private void ShowStartText() {
		if (!showStartText) {
            showStartText = true;
            StartCoroutine(EnableStartText());
		}
	}

    private IEnumerator EnableStartText() {
        UIController.Instance.StartTransition();
        yield return new WaitForSeconds(0.5f);
        UIController.Instance.EnableMenu(false);
        yield return new WaitForSeconds(0.4f);
        UIController.Instance.EnableIntroText(true);
    }

    public void ResetLevel() {
        StartCoroutine(ResetLevelEffect());
    }

    private IEnumerator ResetLevelEffect() {
        UIController.Instance.StartTransition();
        canMove = false;
        yield return null;
        PlayerController.Instance.Reset(levels[currentLevel].playerStartPos);
        canMove = true;
    }

    public void IncrementLevel() {
        StartCoroutine(IncrementLevelEffect());
    }

    private IEnumerator IncrementLevelEffect() {
        // TODO: Add transition effect
        UIController.Instance.StartTransition();
        if(currentLevel + 1 >= levels.Length) {
            GameOver();
		} else {
            yield return new WaitForSeconds(0.5f);
            currentLevel++;
            UpdateLevelVisibility();
            ResetLevel();
        }
    }

    //Checks if player is within level bounds
    private bool WithinLevel() {
        Transform playerPos = PlayerController.Instance.transform;
        if (Mathf.Abs(playerPos.position.x) > 10) return false;
        if (Mathf.Abs(playerPos.position.y) > 10) return false;
        return true;
    }

    private void UpdateBackgroundMusic() {
        if (transitioningMusic) return;

        if ((isMainMenu || (!isMainMenu && showEndText)) && audioSource.clip != windTrack) {
            StartCoroutine(FadeTracks(windTrack));
        } else if (!isMainMenu && audioSource.clip != mainTrack) {
            StartCoroutine(FadeTracks(mainTrack));
        }
    }

    IEnumerator FadeTracks(AudioClip newClip) {
        transitioningMusic = true;
        float t = 1.0f;
        while(t > 0.0f) {
            audioSource.volume = maxVolume * t;
            t -= Time.deltaTime * 2.5f;
            yield return null;
		}

        SwapAudioClip(newClip);

        t = 0.0f;
        while (t < 1.0f) {
            audioSource.volume = maxVolume * t;
            t += Time.deltaTime * 2.5f;
            yield return null;
        }

        transitioningMusic = false;
        yield return null;
	}

    private void SwapAudioClip(AudioClip newClip) {
        audioSource.Stop();
        audioSource.clip = newClip;
        audioSource.Play();
    }

    private void ShowEndText() {
        if (!showEndText) {
            showEndText = true;
            StartCoroutine(EnableEndText());
        }
    }

    private IEnumerator EnableEndText() {
        UIController.Instance.StartTransition();
        yield return new WaitForSeconds(0.6f);
        UIController.Instance.EnableOutroText(true);
    }

    public void GameOver() {
        PlayerController.Instance.gameObject.transform.position = new Vector2(100, 100);
        if (!isMainMenu) {
			if (!showEndText) {
                ShowEndText();
			} else {
                isMainMenu = true;
                StartCoroutine(GameOverTransition());
            }
        }
        UpdateBackgroundMusic();
    }

    private IEnumerator GameOverTransition() {
        UIController.Instance.StartTransition();
        yield return new WaitForSeconds(0.5f);
        showEndText = false;
        UIController.Instance.EnableOutroText(false);
        UIController.Instance.EnableMenu(true);
    }

    public void StartGame() {
		if (isMainMenu) {
            isMainMenu = false;
            StartCoroutine(StartGameTransition());
        }
	}

    private IEnumerator StartGameTransition() {
        UIController.Instance.StartTransition();
        yield return new WaitForSeconds(0.5f);
        currentLevel = 0;
        showStartText = false;
        UIController.Instance.EnableIntroText(false);
        UpdateLevelVisibility();
        ResetLevel();
        UIController.Instance.EnableMenu(false);
        UpdateBackgroundMusic();

    }

    private void UpdateLevelVisibility() {
        for(int i = 0; i < levels.Length; i++) {
            levels[i].levelContainer.SetActive(currentLevel == i);
		}
	}

    public bool CanMove => canMove;

    public int CurrentLevel => currentLevel;
    public int GetLevelsLength => levels.Length;
}
